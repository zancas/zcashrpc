# zcashrpc

This cargo package provides a few libraries useful for implementing a
`zcashd` RPC client using the `tokio` async I/O framework. It also
includes some utility binaries.

## License

This code is licensed under the MIT license. See `./LICENSE.txt`.

## Security & Safety

This code has not been audited, and is currently a part time hobby project.

## Get Started

`cargo run` runs the "smoke tests" for this to work you'll need a connection to
a running `zcashd` instance that can support RPC calls.

